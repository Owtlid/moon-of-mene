class Character {
  name = "";
  role = "";
  damage = 0;
  armor = 0;
  damageType = "";
  constructor(name, role, damage, armor) {
    this.name = name;
    this.role = role;
    this.damage = damage;
    this.armor = armor;
  }
  Armorr() {
    console.log(`Armor:${this.armor}`);
  }
  Damagee() {
    this.damageType =
      this.role === "Mage" || this.role === "Support" ? "Magical" : "Attack";
    console.log(`DamageType:${this.damageType}`);
    console.log(`Damage:${this.damage}`);
  }
}

class Mage extends Character {
  magicPower = 0;
  constructor(name, damage, armor, magicPower) {
    super(name, "Mage", damage, armor);
    this.magicPower = magicPower;
  }
  Fly() {
    console.log(`isAbleToFly:True`);
  }
}

class Support extends Character {
  heal = 0;
  constructor(name, damage, armor, heal) {
    super(name, "Support", damage, armor);
    this.heal = heal;
  }
  Healer() {
    console.log(`Heal:${this.heal}`);
  }
}

class Adc extends Character {
  range = 0;
  constructor(name, damage, armor, range) {
    super(name, "Adc", damage, armor);
    this.range = range;
  }
  calcRange(x) {
    console.log(x <= 30 ? "Melee" : "Ranged");
  }
}

let aa = new Adc("Ashley", 140, 4, 20);
let sup = new Support("Richie", 90, 2, 90);
let mag = new Mage("Ellie", 250, 4, 150);
let char = new Character("GUJA", "Tank", 250, 10);

console.log("=====================");
char.Armorr();
char.Damagee();
console.log("=====================");
aa.calcRange(20);
console.log("=====================");
sup.Healer();
console.log("=====================");
mag.Fly();
console.log("=====================");

//   printInfo(){
//       this.damageType = this.role === "Mage" || this.role === "Support" ? "Magical" : "Attack";
//       this.isAbleToFly = this.role === "Mage" || this.role === "Support";
//       console.log(`Name:${this.name}  Role:${this.role}`);
//       console.log(`DamageType:${this.damageType}`);
//       console.log(`DPS:${this.damage}`);
//       console.log(`Armor:${this.armor}`);
//       console.log(`isAbleToFly:${this.isAbleToFly}`);
//   }
